﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Joystick : MonoBehaviour
{
    public Transform player;
    public float speed = 5.0f;
    private bool touchStart = false;
    private Vector2 pointA;
    private Vector2 pointB;

    public Transform circle;
    public Transform outerCircle;

    public static Joystick instance;
    public bool canMove;
    private void Start()
    {
        if(instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(this.gameObject);
        }
    }

    private void OnDestroy()
    {
        if(instance == this)
        {
            instance = null;
        }
    }

    public void setTouchStart(bool in_status)
    {
        touchStart = in_status;
    }

    public bool getTouchStart()
    {
        return touchStart;
    }
    // Update is called once per frame
    void Update()
    {

        if(!canMove)
        {
            //return;
        }


        if (Input.GetMouseButtonDown(0))
        {
            pointA = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, Camera.main.transform.position.z));

        }
        if (Input.GetMouseButton(0))
        {
            touchStart = true;
            pointB = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, Camera.main.transform.position.z));
        }
        else
        {

            if(touchStart)
            {
              GetComponent<Detection>().DetectEnemy();
              player.GetComponent<Rigidbody>().velocity = Vector3.zero;
              touchStart = false;
              circle.transform.position = outerCircle.transform.position;
              canMove = false;
            }
        }

    }
    private void FixedUpdate()
    {
        if (touchStart)
        {
            Vector2 offset = pointB - pointA;
            Vector2 direction = Vector2.ClampMagnitude(offset, 1.0f);
            moveCharacter(direction);

            circle.transform.position = new Vector2(outerCircle.transform.position.x + direction.x, outerCircle.transform.position.y + direction.y);

            if(GetComponent<Detection>().closestTarget != null)
            {
                GetComponent<Detection>().closestTarget = null;
                GetComponent<Shooting>().target = null;
            }
        }
        else
        {
            
        }

    }
    void moveCharacter(Vector2 direction)
    {
        player.GetComponent<Rigidbody>().velocity = (new Vector3 (direction.x,direction.y,0) * speed * Time.deltaTime);
    }
}