﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public enum Abilites
{
    multiShot,
    explosive,
    ricochet,
    diagonal,
    powerup
}

[RequireComponent(typeof(Shooting))]
[RequireComponent(typeof(Detection))]
[RequireComponent(typeof(Joystick))]
public class BasePlayer : MonoBehaviour
{

    public int maxHealth;
    public int health;
    public SpriteRenderer healthBar;
    float maxHealthBarSize;
    public List<Abilites> abilityList;
    public int damage;
    // Start is called before the first frame update
    void Start()
    {
        health = maxHealth;
        maxHealthBarSize = 5.3f;
        if(abilityList != null && abilityList.Contains(Abilites.powerup))
        {
            damage *= 2;
        }
    }

    // Update is called once per frame
    void Update()
    {
        //if(Input.GetKeyDown(KeyCode.K))
        //{
        //    OnTakeDamage(10);
        //}
    }

    public void OnTakeDamage(int damage)
    {
        if(health - damage <=  0)
        {
            //do death stuff
            return;
        }
        
        health -= damage;
        float healthPercentage = (float)health / (float)maxHealth * 100;
        healthBar.size = new Vector2(healthPercentage / 100 * maxHealthBarSize, 3.6f);

    }
}
