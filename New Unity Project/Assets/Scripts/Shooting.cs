﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class Shooting : MonoBehaviour
{
    public Transform target;
    public GameObject obj_Arrow;
    public Transform forwardShootPos, leftShootPos, rightShootPos;
    public float rateOfFire;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetTarget(Transform in_target)
    {
        target = in_target;
        StartCoroutine("Shoot");
    }

    IEnumerator Shoot()
    {
       if(target == null)
        {
            StopAllCoroutines();
        }


        if(GetComponent<BasePlayer>() != null && GetComponent<BasePlayer>().abilityList.Contains(Abilites.multiShot))
        {

        }
        else
        {
            GameObject obj = (GameObject)Instantiate(obj_Arrow);
            obj.GetComponent<Arrow>().originator = this.gameObject;
            obj.GetComponent<Collider>().enabled = true;
            int damage = GetComponent<BasePlayer>() != null ? GetComponent<BasePlayer>().damage : GetComponent<BaseEnemy>().damage;
            obj.GetComponent<Arrow>().damage = damage;
            if(gameObject.tag == "Enemy")
            {
                obj.transform.position = transform.position;
                obj.transform.rotation = transform.rotation;
            }
            else
            {
                obj.transform.position = forwardShootPos.position;
                obj.transform.rotation = forwardShootPos.rotation;
            }
           
        }
        yield return new WaitForSeconds(rateOfFire);
        if(target != null)
        {
            StartCoroutine("Shoot");
        }
    }

    void SetPos()
    {
        //for ability
    }

}
