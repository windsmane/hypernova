﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Shooting))]
public class BaseEnemy : MonoBehaviour
{

    public int maxHealth;
    public int health;
    public int damage;
    public GameObject Player;
    public SpriteRenderer healthBar;
    float maxHealthBarSize;

    public GameObject Collectible;
    // Start is called before the first frame update
    void Start()
    {
        health = maxHealth;
        maxHealthBarSize = 5.3f;
        GetComponent<Shooting>().SetTarget(Player.transform);
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 lookPos = Player.transform.position - transform.position;
        Quaternion rotation = Quaternion.LookRotation(lookPos);
        transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime * 15f);
    }

    public void OnTakeDamage(int damage)
    {
        if (health - damage <= 0)
        {
            int numberOfCollectibles = Random.Range(1, 10);
            for(int i = 0; i <= numberOfCollectibles; i++)
            {
                MeshRenderer m_renderer = GetComponent<MeshRenderer>();
                float x = Random.Range(m_renderer.bounds.min.x, m_renderer.bounds.max.x);
                float y = Random.Range(m_renderer.bounds.min.y, m_renderer.bounds.max.y);
                Vector3 randomPos = new Vector3(x, y, transform.position.z);
                Instantiate(Collectible,randomPos,Quaternion.identity);
            }
            Player.GetComponent<Detection>().AfterEnemyDeath(this.gameObject);
            Destroy(this.gameObject);
            return;
        }

        health -= damage;
        float healthPercentage = (float)health / (float)maxHealth * 100;
        healthBar.size = new Vector2(healthPercentage / 100 * maxHealthBarSize, 3.6f);

    }
}
