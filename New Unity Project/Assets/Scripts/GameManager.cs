﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{

    public int coincount;
    public static GameManager instance;
    // Start is called before the first frame update
    void Start()
    {
        if(instance != null)
        {
            Destroy(this.gameObject);
        }

        instance = this;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void IncreaseCoinCount()
    {
        coincount++;
    }
}
