﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arrow : MonoBehaviour
{
    public int damage;
    public int speed;
    public GameObject originator;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.position =  Vector3.Lerp(transform.position, transform.position + transform.forward, speed * Time.deltaTime);
        //Debug.DrawRay(transform.position, transform.forward, Color.red, 10f);
       
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject != originator)
        {
            switch (other.gameObject.tag)
            {
                case "Enemy":
                    other.gameObject.GetComponent<BaseEnemy>().OnTakeDamage(damage);
                    Destroy(this.gameObject);
                    break;
                case "Wall":
                    Destroy(this.gameObject);
                    break;
                case "Player":
                    other.gameObject.GetComponent<BasePlayer>().OnTakeDamage(damage);
                    Destroy(this.gameObject);
                    break;

            }
        }
        
        
    }
}
