﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Detection : MonoBehaviour
{
    public List<GameObject> target;
    public Transform closestTarget;
    // Start is called before the first frame update
    void Start()
    {
        GameObject[] t_target = GameObject.FindGameObjectsWithTag("Enemy");
        foreach (GameObject obj in t_target)
        {
            target.Add(obj);
        }

        DetectEnemy();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void AfterEnemyDeath(GameObject deadEnemy)
    {
        target.Remove(deadEnemy);
        closestTarget = null;
        GetComponent<Shooting>().target = null;
        if(target.Count == 0)
        {
            //LevelEndStuff;
            return;
        }
        Invoke("DetectEnemy", 0.5f);
    }
    public void DetectEnemy()
    {
        float distance = 100000;
        for (int i = 0; i < target.Count; i++)
        {
            float temp_distance = Vector3.Distance(transform.position, target[i].transform.position);
            if (temp_distance < distance)
            {
                distance = temp_distance;
                closestTarget = target[i].transform;
            }
        }
        InvokeRepeating("TurnToEnemy", 0.1f, 0.1f * Time.deltaTime);
      
    }

    void TurnToEnemy()
    {
        if (closestTarget == null)
        {
            return;
        }
        Vector3 targetpos = closestTarget.position - transform.position;
        Debug.Log(targetpos.x);
        int dir = targetpos.x > 0 ? -1 : 1;
        transform.Rotate(transform.forward, dir);
        //
        if (Vector3.Angle(targetpos, transform.up) < 1.0f)
        {
            GetComponent<Shooting>().SetTarget(closestTarget);
            CancelInvoke("TurnToEnemy");
        }
    }
}
